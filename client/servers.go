package servers

import (
	"bytes"
	"encoding/json"
	"fmt"
	"msbigtech/internal/generated"
	"net/http"
)

const host = "http://msbigtech.local"
const apiPrefix = "/api/v1/servers"

func createServer(httpClient *http.Client, serverName string) (*generated.CreateServerResponse, error) {
	uri := fmt.Sprintf("%s/%s/%s", host, apiPrefix, "")

	var request = generated.CreateServerRequest{
		Name: serverName,
	}

	body := bytes.NewBuffer(nil)
	if err := json.NewEncoder(body).Encode(request); err != nil {
		return nil, err
	}

	httpRequest, err := http.NewRequest(http.MethodPost, uri, body)
	if err != nil {
		return nil, err
	}

	httpResponse, err := httpClient.Do(httpRequest)
	if err != nil {
		return nil, err
	}

	if httpResponse.StatusCode != http.StatusCreated {
		var errorMsg generated.ErrorResponse
		if err := json.NewDecoder(httpResponse.Body).Decode(&errorMsg); err != nil {
			return nil, fmt.Errorf("server not created: status code %d", httpResponse.StatusCode)
		}

		return nil, fmt.Errorf("server not created: status code %d, error: %s", httpResponse.StatusCode, errorMsg.Message)
	}

	response := new(generated.CreateServerResponse)
	if err := json.NewDecoder(httpResponse.Body).Decode(response); err != nil {
		return nil, fmt.Errorf("can't unmarshal reponse: %s", err.Error())
	}

	return response, nil
}

func searchServers(httpClient *http.Client, serverName string) (*generated.SearchServersResponse, error) {
	uri := fmt.Sprintf("%s/%s/%s", host, apiPrefix, "search")

	var request = generated.SearchServersRequest{
		ServerName: serverName,
	}

	body := bytes.NewBuffer(nil)
	if err := json.NewEncoder(body).Encode(request); err != nil {
		return nil, err
	}

	httpRequest, err := http.NewRequest(http.MethodPost, uri, body)
	if err != nil {
		return nil, err
	}

	httpResponse, err := httpClient.Do(httpRequest)
	if err != nil {
		return nil, err
	}

	response := new(generated.SearchServersResponse)
	if err := json.NewDecoder(httpResponse.Body).Decode(response); err != nil {
		return nil, fmt.Errorf("can't unmarshal reponse: %s", err.Error())
	}

	return response, nil
}
