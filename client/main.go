package servers

import (
	"log"
	"net/http"
)

func createServerTest() {
	httpClient := &http.Client{}

	resp, err := createServer(httpClient, "Server #1")

	if err != nil {
		log.Printf("createServer error: %v", err)
	} else {
		log.Printf("createServer: %#v", resp)
	}
}

func searchServerTest() {
	httpClient := &http.Client{}

	resp, err := searchServers(httpClient, "Server")

	if err != nil {
		log.Printf("searchServers error: %v", err)
	} else {
		log.Printf("searchServers: %#v", resp)
	}
}

func main() {
	createServerTest()
	searchServerTest()
}
