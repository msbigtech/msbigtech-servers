package handlers

import (
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	_ "github.com/swaggo/files"
	_ "github.com/swaggo/gin-swagger"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"msbigtech/internal/models/requests"
	"msbigtech/internal/models/responses"
	"net/http"
	"os"
	"strconv"
)

func httpErrorMsg(err error) *responses.ErrorResponse {
	if err == nil {
		return nil
	}

	return &responses.ErrorResponse{
		Message: err.Error(),
	}
}

// LivenessProbeHandler godoc
// @Summary      liveness probe
// @Tags         healthchecks
// @Accept       json
// @Produce      json
// @Success      200  {string}  string    "ok"
// @Router       /live [get]
func LivenessProbeHandler(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{
		"response": "ok",
	})
}

// ReadinessProbeHandler godoc
// @Summary      readiness probe
// @Tags         healthchecks
// @Accept       json
// @Produce      json
// @Success      200  {string}  string    "ok"
// @Router       /ready [get]
func ReadinessProbeHandler(c *gin.Context) {
	dsn := fmt.Sprintf(
		"host=%s user=%s password=%s dbname=%s port=%s sslmode=prefer TimeZone=Europe/Moscow",
		os.Getenv("DB_HOST"),
		os.Getenv("DB_USER"),
		os.Getenv("DB_PASSWORD"),
		os.Getenv("DB_NAME"),
		os.Getenv("DB_PORT"),
	)
	_, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})

	if err != nil {
		panic("Failed to connect to database!")
	}

	c.JSON(http.StatusOK, gin.H{
		"response": "ok",
	})
}

// GetCurrentUserServersHandler godoc
// @Summary      Возвращает серверы текущего пользователя
// @Tags         servers
// @Accept       json
// @Produce      json
// @Security     MSBigtechAuth[read:servers]
// @Success      200  {object} responses.GetCurrentUserServersResponse "successful operation"
// @Router       / [get]
func GetCurrentUserServersHandler(c *gin.Context) {
	// TODO

	result := responses.GetCurrentUserServersResponse{}

	c.JSON(http.StatusOK, result)
}

// CreateServerHandler godoc
// @Summary      Создает сервер
// @Tags         servers
// @Accept       json
// @Produce      json
// @Security     MSBigtechAuth[write:servers]
// @Param		 data	body	requests.CreateServerRequest  true "CreateServerRequest"
// @Success      200  {object} 	responses.CreateServerResponse "successful operation"
// @Failure		 400  {object}	responses.ErrorResponse "invalid input"
// @Failure		 422  {object}	responses.ErrorResponse "validation exception"
// @Router       / [post]
func CreateServerHandler(c *gin.Context) {
	var request requests.CreateServerRequest

	if err := json.NewDecoder(c.Request.Body).Decode(&request); err != nil {
		c.JSON(http.StatusBadRequest, httpErrorMsg(err))
	}

	// TODO

	response := responses.CreateServerResponse{}

	c.JSON(http.StatusCreated, response)
}

// SearchServersHandler godoc
// @Summary      Выполняет поиск серверов
// @Tags         servers
// @Accept       json
// @Produce      json
// @Security     MSBigtechAuth[read:servers]
// @Param		 data	body	requests.SearchServersRequest  true "SearchServersRequest"
// @Success      200  {object} 	responses.SearchServersResponse "successful operation"
// @Failure		 400  {object}	responses.ErrorResponse "invalid input"
// @Failure		 422  {object}	responses.ErrorResponse "validation exception"
// @Router       /search [post]
func SearchServersHandler(c *gin.Context) {
	var request requests.SearchServersRequest

	if err := json.NewDecoder(c.Request.Body).Decode(&request); err != nil {
		c.JSON(http.StatusBadRequest, httpErrorMsg(err))
	}

	// TODO

	response := responses.SearchServersResponse{}

	c.JSON(http.StatusOK, response)
}

// CreateServerChannelHandler godoc
// @Summary      Создает канал на сервере
// @Tags         channels
// @Accept       json
// @Produce      json
// @Security     MSBigtechAuth[read:channels]
// @Param		 data	body	requests.CreateServerChannelRequest  true "CreateServerRequest"
// @Success      200  {object} 	responses.CreateServerChannelResponse "successful operation"
// @Failure		 400  {object}	responses.ErrorResponse "invalid input"
// @Failure		 422  {object}	responses.ErrorResponse "validation exception"
// @Router       /channels [post]
func CreateServerChannelHandler(c *gin.Context) {
	var request requests.CreateServerChannelRequest

	if err := json.NewDecoder(c.Request.Body).Decode(&request); err != nil {
		c.JSON(http.StatusBadRequest, httpErrorMsg(err))
	}

	// TODO

	response := responses.CreateServerChannelResponse{}

	c.JSON(http.StatusCreated, response)
}

// GetServerChannelHandler godoc
// @Summary      Возвращает канал на сервере
// @Tags         channels
// @Accept       json
// @Produce      json
// @Security     MSBigtechAuth[read:channels]
// @Param		 channelId		path	int		true	"ID of channel"
// @Success      200  {object} 	responses.GetServerChannelResponse "successful operation"
// @Failure		 400  {object}	responses.ErrorResponse "invalid input"
// @Router       /channels/{channelId} [get]
func GetServerChannelHandler(c *gin.Context) {
	// TODO

	channelId, err := strconv.ParseInt(c.Param("channelId"), 10, 64)

	if err != nil {
		c.JSON(http.StatusBadRequest, httpErrorMsg(err))
	}

	response := responses.GetServerChannelResponse{ID: channelId}

	c.JSON(http.StatusOK, response)
}

// DeleteServerChannelHandler godoc
// @Summary      Удаляет канал на сервере
// @Tags         channels
// @Accept       json
// @Produce      json
// @Security     MSBigtechAuth[delete:channels]
// @Param		 channelId		path	int		true	"ID of channel"
// @Success      200  {string} 	"ok" "successful operation"
// @Failure		 400  {object}	responses.ErrorResponse "invalid input"
// @Router       /channels/{channelId} [delete]
func DeleteServerChannelHandler(c *gin.Context) {
	// TODO

	_, err := strconv.ParseInt(c.Param("channelId"), 10, 64)

	if err != nil {
		c.JSON(http.StatusBadRequest, httpErrorMsg(err))
	}

	c.JSON(http.StatusOK, "")
}

// SubscribeServerChannelHandler godoc
// @Summary      Подписывается на канал на сервере
// @Tags         channels
// @Accept       json
// @Produce      json
// @Security     MSBigtechAuth[read:channels]
// @Param		 channelId		path	int		true	"ID of channel"
// @Success      200  {string} 	"ok" "successful operation"
// @Failure		 400  {object}	responses.ErrorResponse "invalid input"
// @Router       /channels/{id}/subscribe [post]
func SubscribeServerChannelHandler(c *gin.Context) {
	// TODO

	_, err := strconv.ParseInt(c.Param("channelId"), 10, 64)

	if err != nil {
		c.JSON(http.StatusBadRequest, httpErrorMsg(err))
	}

	// TODO

	c.JSON(http.StatusOK, "")
}

// UnsubscribeServerChannelHandler godoc
// @Summary      Отписывается от канала на сервере
// @Tags         channels
// @Accept       json
// @Produce      json
// @Security     MSBigtechAuth[read:channels]
// @Param		 channelId		path	int		true	"ID of channel"
// @Success      200  {string} 	"ok" "successful operation"
// @Failure		 400  {object}	responses.ErrorResponse "invalid input"
// @Router       /channels/{id}/unsubscribe [post]
func UnsubscribeServerChannelHandler(c *gin.Context) {
	// TODO

	_, err := strconv.ParseInt(c.Param("channelId"), 10, 64)

	if err != nil {
		c.JSON(http.StatusBadRequest, httpErrorMsg(err))
	}

	// TODO

	c.JSON(http.StatusOK, "")
}
