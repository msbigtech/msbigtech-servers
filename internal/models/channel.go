package models

// Channel channel
//
// swagger:model Channel
type Channel struct {

	// id
	// Example: 10
	ID int64 `json:"id,omitempty"`

	// name
	Name string `json:"name,omitempty"`

	// owner
	Owner *User `json:"owner,omitempty"`

	// server
	Server *Server `json:"server,omitempty"`
}
