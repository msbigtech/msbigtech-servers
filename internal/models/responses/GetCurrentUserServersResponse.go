package responses

import "msbigtech/internal/models"

// GetCurrentUserServersResponse get current user response
//
// swagger:model GetCurrentUserServersResponse
type GetCurrentUserServersResponse struct {
	// items
	Items []*models.Server `json:"items"`
}
