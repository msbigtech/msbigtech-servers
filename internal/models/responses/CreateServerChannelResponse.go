package responses

import "msbigtech/internal/models"

// CreateServerChannelResponse create server channel response
//
// swagger:model CreateServerChannelResponse
type CreateServerChannelResponse struct {

	// id
	// Example: 10
	ID int64 `json:"id,omitempty"`

	// name
	Name string `json:"name,omitempty"`

	// owner
	Owner *models.User `json:"owner,omitempty"`

	// server
	Server *models.Server `json:"server,omitempty"`
}
