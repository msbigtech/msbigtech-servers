package responses

import "msbigtech/internal/models"

// SearchServersResponse search servers response
//
// swagger:model SearchServersResponse
type SearchServersResponse struct {

	// items
	Items []*models.Server `json:"items"`
}
