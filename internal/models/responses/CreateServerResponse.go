package responses

import "msbigtech/internal/models"

// CreateServerResponse create server response
//
// swagger:model CreateServerResponse
type CreateServerResponse struct {

	// channels
	Channels []*models.Channel `json:"channels"`

	// id
	// Example: 10
	ID int64 `json:"id,omitempty"`

	// name
	Name string `json:"name,omitempty"`

	// owner
	Owner *models.User `json:"owner,omitempty"`
}
