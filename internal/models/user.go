package models

// User user
//
// swagger:model User
type User struct {

	// id
	// Example: 10
	ID int64 `json:"id,omitempty"`

	// nickname
	Nickname string `json:"nickname,omitempty"`
}
