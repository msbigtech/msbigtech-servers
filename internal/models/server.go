package models

// Server server
//
// swagger:model Server
type Server struct {

	// channels
	Channels []*Channel `json:"channels"`

	// id
	// Example: 10
	ID int64 `json:"id,omitempty"`

	// name
	Name string `json:"name,omitempty"`

	// owner
	Owner *User `json:"owner,omitempty"`
}
