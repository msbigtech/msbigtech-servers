package requests

// SearchServersRequest search servers request
//
// swagger:model SearchServersRequest
type SearchServersRequest struct {

	// server name
	ServerName string `json:"serverName,omitempty"`
}
