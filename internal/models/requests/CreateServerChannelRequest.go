package requests

// CreateServerChannelRequest create server channel request
//
// swagger:model CreateServerChannelRequest
type CreateServerChannelRequest struct {

	// name
	Name string `json:"name,omitempty"`

	// server Id
	ServerID int64 `json:"serverId,omitempty"`
}
