package requests

// CreateServerRequest create server request
//
// swagger:model CreateServerRequest
type CreateServerRequest struct {

	// name
	Name string `json:"name,omitempty"`
}
