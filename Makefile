run:
	go run cmd/main.go

docs-gen:
	swag init -d cmd,internal/handlers,internal/models -o docs