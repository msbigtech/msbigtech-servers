package main

import (
	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
	_ "github.com/swaggo/files"
	_ "github.com/swaggo/gin-swagger"
	"msbigtech/internal/handlers"
)

// @title           MSBigtech Servers API
// @version         1.0

// @host      msbigtech.local
// @BasePath /v1
// @schemes http https

// @tag.name servers
// @tag.description Servers API
// @tag.name channels
// @tag.description Channels API
// @tag.name healthchecks
// @tag.description Healthchecks API

// @securitydefinitions.oauth2.implicit MSBigtechAuth
// @authorizationUrl https://msbigtech.local/oauth/authorize
// @scope.write:servers modify servers
// @scope.read:servers read your servers
// @scope.write:channels write channels
// @scope.read:channels read server channels
// @scope.delete:channels delete server channels
// @scope.join:channels join server channels

// @securitydefinitions.apikey api_key
// @in header
// @name api_key

func setupRouter() *gin.Engine {
	r := gin.Default()

	r.GET("/live", handlers.LivenessProbeHandler)
	r.GET("/ready", handlers.ReadinessProbeHandler)

	r.GET("/", handlers.GetCurrentUserServersHandler)
	r.POST("/", handlers.CreateServerHandler)
	r.POST("/search", handlers.SearchServersHandler)

	r.POST("/channels", handlers.CreateServerChannelHandler)
	r.GET("/channels/:channelId", handlers.GetServerChannelHandler)
	r.DELETE("/channels/:channelId", handlers.DeleteServerChannelHandler)
	r.POST("/channels/:channelId/subscribe", handlers.SubscribeServerChannelHandler)
	r.POST("/channels/:channelId/unsubscribe", handlers.UnsubscribeServerChannelHandler)

	return r
}

func goDotEnvVariable() {
	godotenv.Load(".env")
}

func main() {
	goDotEnvVariable()

	r := setupRouter()

	r.Run(":8080")
}
