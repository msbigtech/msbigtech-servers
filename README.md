# msbigtech-servers

```shell
docker compose build
docker compose up

docker rm $(docker ps -a | grep postgres | awk '{print $1}')
docker volume rm msbigtech-servers_db-data
```